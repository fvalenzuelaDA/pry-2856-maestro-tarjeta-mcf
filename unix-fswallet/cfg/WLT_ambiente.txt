######## RUTAS ############

#HOME
DirIni=$HOME

# Directorio de archivos de shell
DirShe=${DirIni}/she
export DirShe

# Directorio de archivos de log
DirLog=${DirIni}/log
export DirLog

# Directorio de archivos de entrada
DirIn=${DirIni}
export DirIn

# Directorio de archivos temporales
DirTmp=${DirIni}/tmp
export DirTmp

# Directorio de archivos de salida
DirOut=${DirIni}/out
export DirOut

# Directorio de fuentes sql
DirFue=${DirIni}/fue
export DirFue

# Directorio de archivos de respaldo
DirBkp=${DirIni}/bkp
export DirBkp

# Directorio de archivos de respaldo
DirDat=${DirIni}/dat
export DirDat

# Directorio de archivos de respaldo
DirCfg=${DirIni}/cfg
export DirCfg

# Directorio de archivos de formato bcp (FMT)
DirFmt=${DirIni}/fmt
export DirFmt

#Ruta archivo de funciones globales
RutaFunc=${DirCfg}/WLT_funciones.txt
export RutaFunc

######### VARIABLES GOBALES ################

#Bases de datos wallet
bd=db_maestro_tarjetas
export bd

#uypuser para realizar conexion
UyPuser=wallet
export UyPuser

arch_ibm=MCF_TDCODFT1
export arch_ibm

ext_ibm=DAT
export ext_ibm
