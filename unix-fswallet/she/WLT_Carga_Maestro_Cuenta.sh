#!/usr/bin/sh
#************************************************************************
# Nombre				: WLT_Carga_Maestro_Cuenta.sh
# Ruta					: /she/
# Autor					: Sebastian Rozas (Ada) - Ing. SW BCI: Felipe Arraño
# Fecha					: 26/02/2019
# Descripción			: Carga de tabla
# Parámetros de Entrada	: <AAAAMMDD que se cargara>
# Ejemplo de ejecución	: sh WLT_Carga_Maestro_Cuenta.sh 20190101
#************************************************************************
#																		
# Modificaciónn			: Archivo de entrada vien con un nuevo campo		 
#						  "tipo de bloqueo de tarjeta", por lo cual, este
#						  se debe considerar e la carga a la bd sql server
# Autor					: Ada Ltda.
# Fecha modificación	: 2021-10-06
#************************************************************************

sNOMBRE_SHELL="WLT_Carga_Maestro_Cuenta.sh"

sLOG=$HOME/log
sCFG=$HOME/cfg

printlog()
{
	Hora=`date +%H:%M:%S`
	echo ${Hora} "$@" # Salida a Consola
	echo ${Hora} "$@"  >> $sLOG/${SHELL_LOG}
}

SHELL_LOG=`date +'%Y%m%d'.WLT_Carga_Maestro_Cuenta.${1}.log`
ARCHLOG=`date +'%Y%m%d'.WLT_Carga_Maestro_Cuenta.${1}`
date '+  %Y-%m-%d %H:%M:%S' >> $sLOG/$ARCHLOG
server=`uname -n`
dia=`date +'%Y'/'%m'/'%d'`
Hora=`date +'%H':'%M':'%S'`
AMB=`BciAmbiente`
RutaAmbiente="$HOME/cfg/WLT_ambiente.txt"
INF_PGM=`echo $0 |sed s/"\/"/" "/g|awk '{print $(NF)}'`
INF_PID=$$
INF_INI=`date +%d/%m/%Y" "%H:%M:%S`
INF_ARG=$@

SALIR()
{
	# $1 Codigo de salida
	# $2 o mas, mensaje de salida
	printlog "if [ $1 -gt 0 ]"
	if [ $1 -gt 0 ]
	then
		printlog "La funcion salir recibe un error como codigo de salida. Codigo recibido $1"
	fi
	printlog "************************************************************************************"
	printlog "**                     RESUMEN                                                    **"
	printlog "************************************************************************************"
	printlog "Servidor      : `uname -n`"
	printlog "Usuario       : `whoami`" 
	printlog "Programa      : ${INF_PGM}"
	printlog "ID proceso    : ${INF_PID}"
	printlog "Hora_Inicio   : ${INF_INI}"
	printlog "Hora Termino  : `date +%d/%m/%y\" \"%H:%M:%S`"
	printlog "Parametros    : ${INF_ARG}"
	printlog "Cant. Parametr: ${INF_CANT_ARG}"
	printlog "Resultado     : ${2} "
	printlog "Archivo Log   : ${sLOG}/${ARCHLOG}"
	printlog "************************************************************************************"
	exit $1
}


###################################################################
#					Ejecuta archivo ambiente  					  #
#	    Valida la existencia del archivo de Configuracion 		  #
###################################################################

printlog "Valida existencia de archivo: $RutaAmbiente "
if [ ! -f $RutaAmbiente ]
then 
	echo " "
	echo "ERROR: No se encuentra el archivo $RutaAmbiente"
	echo " "
	SALIR 2 "ERROR: No se encuentra el archivo $RutaAmbiente"
fi
printlog "Valida contenido de archivo: $RutaAmbiente"
if test -s $RutaAmbiente
then
	echo "Archivo $RutaAmbiente OK"
	. $RutaAmbiente
else
	echo "Error, Archivo sin datos: $RutaAmbiente"
	SALIR 2 "Error, Archivo sin datos: $RutaAmbiente"
fi
printlog "Valida existencia de archivo: $RutaAmbiente "
if [ ! -f $RutaFunc ]
then 
	echo " "
	echo "ERROR: No se encuentra el archivo $RutaFunc"
	echo " "
	SALIR 2 "ERROR: No se encuentra el archivo $RutaFunc"
fi
printlog "Valida contenido de archivo: $RutaAmbiente"
if test -s $RutaFunc
then
	echo "Archivo $RutaFunc OK"
	. $RutaFunc
else
	echo "Error, Archivo sin datos: $RutaFunc"
	SALIR 2 "Error, Archivo sin datos: $RutaFunc"
fi

Eliminar_Archivo $DirLog/$SHELL_LOG
Eliminar_Archivo $DirLog/$ARCHLOG

sFecha=`date '+%d/%m/%Y'`
sHora=`date '+%X'`

#---------------------------
Realiza_Conexion ()
{

#---------------------------------------------------------------------
#  OBTIENE DATOS DE CONEXION A LA BASE DE DATOS
#---------------------------------------------------------------------

printlog " "
printlog "*********************************"
printlog "*   OBTTENE DATOS DE CONEXION   *"
printlog "*       A LA BASE DE DATOS      *"
printlog "*********************************"
echo " "
Mensaje "--> Datos de conexion a BD:"

Mensaje "Obteniendo Usuario y Password para ambiente sybase..."
printlog "Obteniendo Usuario y Password para ambiente sybase..."


for uyprsp in `UyPuser $UyPuser`
do
   uypDa[$uypcnt]=$uyprsp
      (( uypcnt=$uypcnt+1))
done
uypcnt=0
printlog "if [ ${uypDa[0]} = Falla ]"
if [ "${uypDa[0]}" = "Falla" ] ; then
   echo "No se pudo obtener la clave por ${uypDa[1]}"  >> $HOME/log/$SHELL_LOG
   echo "No se pudo obtener la clave por ${uypDa[1]}" 
   SALIR 2 "No se pudo obtener la clave por ${uypDa[1]}"
 else
 
   SYB_U=${uypDa[0]}
   SYB_P=${uypDa[1]}
   
   Mensaje "Usuario y Passwword...OK"
   printlog "Usuario y Passwword...OK"
   
fi
}

Elimina_periodo ()
{

printlog "borrando periodo..."
Mensaje "borrando periodo..."
isql -U${SYB_U} -S${DSQUERY} << eof >> $DirLog/$ARCHLOG
`echo ${SYB_P}`
use $1
go
delete from
	$2
	where
		$3 between '$4' and '$5'
go
eof
#--------------------------------------------------------------------------------
#Siempre validar que el comando se ejecuto en forma correcta
#--------------------------------------------------------------------------------
if [[ $? != 0 ]] then
   Mensaje "  ERROR : La ejecucion del borrado"
   printlog "  ERROR : La ejecucion del borrado"
   exit 2
elif [ -f $DirLog/$ARCHLOG ]; then
   verE=`egrep -c 'Msg |CT-LIBRARY error:|CS-LIBRARY error:' $DirLog/$ARCHLOG `
   if [[ "${verE}" != "0" ]]; then
		Mensaje "  ERROR : Sybase retorno una excepcion"
	cat $DirLog/$ARCHLOG
	  exit 2
   fi
   else
	   Mensaje "  ERROR : No se Elimino el periodo"
	   cat $DirLog/$ARCHLOG
	   exit 2
   fi
printlog "...La ejecucion del borrado OK"
Mensaje "...La ejecucion del borrado OK"
}
#-------------------------

printlog "if [ $# -eq 1 ]"
if [ $# -eq 1 ]
then
	Mensaje "Cantidad de parametros correcta"
	printlog "Cantidad de parametros correcta"
	FecArch=$1
else
	Mensaje "Cantidad de parametros incorrecta"
	printlog "Cantidad de parametros incorrecta"
	SALIR 2 "Cantidad de parametros incorrecta"
fi
	
#Comienza proceso

Valida_Archivo ${DirIn}/${arch_ibm}_${FecArch}.${ext_ibm}

Realiza_Conexion $UyPuser

Ejecuta_Truncate $bd tw_tarjeta_wallet

printlog "Elimina registos duplicados"
Mensaje "Elimina registos duplicados"

sort -u ${DirIn}/${arch_ibm}_${FecArch}.${ext_ibm} > ${DirTmp}/arch_trj_wlt_ORD


printlog "...Elimina registos duplicados OK"
Mensaje "...Elimina registos duplicados OK"


printlog "Formateo de archivo"
Mensaje "Formateo de archivo"

# Eliminacion de caracteres
awk '
BEGIN {
	FS=";";
	tw_num_trj="";
	tw_tipo_trj="";
	tw_fec_ven_trj="";
	tw_rut_habiente="";
	tw_dv_habiente="";
	tw_est_trj="";
	tw_fec_act_trj="";
	tw_cod_bloq_trj="";
	tw_fec_bloq_trj="";
	tw_hra_bloq_trj="";
	tw_num_cta="";
	tw_logo_trj="";
	tw_rut_titular="";
	tw_dv_titular="";
	tw_nom_titular="";
	tw_dir_env_est_cta="";
	tw_com_env_est_cta="";
	tw_ciu_env_est_cta="";
	tw_reg_env_est_cta="";
	tw_cod_blq_cta="";
}
{
	tw_num_trj="XXXXXXXXXXXXXXX"substr($0,16,4);
	tw_tipo_trj=substr($0,21,1);
	tw_fec_ven_trj=substr($0,23,8);
	tw_rut_habiente=substr($0,32,9);
	tw_dv_habiente=substr($0,42,1);
	tw_est_trj=substr($0,44,1);
	tw_fec_act_trj=substr($0,46,8);
	tw_cod_bloq_trj=substr($0,55,1);
	tw_fec_bloq_trj=substr($0,57,8);
	tw_hra_bloq_trj=substr($0,66,6);
	tw_num_cta=substr($0,73,19);
	tw_logo_trj=substr($0,93,3);
	tw_rut_titular=substr($0,97,10);
	tw_dv_titular=substr($0,108,1);
	tw_nom_titular=substr($0,110,40);
	tw_dir_env_est_cta=substr($0,151,40);
	tw_com_env_est_cta=substr($0,192,15);
	tw_ciu_env_est_cta=substr($0,208,15);
	tw_reg_env_est_cta=substr($0,224,2);
	tw_cod_blq_cta=substr($0,227,1);
	
	
	if( tw_fec_ven_trj != "00000000" ){
		fec1=tw_fec_ven_trj
	}else{
		fec1=""
	}
	
	if( tw_fec_act_trj != "00000000" ){
		fec2=tw_fec_act_trj
	}else{
		fec2=""
	}
	
	if( tw_fec_bloq_trj != "00000000" ){
		fec3=tw_fec_bloq_trj
	}else{
		fec3=""
	}
	
	hra=substr(tw_hra_bloq_trj,1,2)":"substr(tw_hra_bloq_trj,3,2)":"substr(tw_hra_bloq_trj,5,2)
	
	printf("%s|%s|%s|%s|%s|",tw_num_trj,tw_tipo_trj,fec1,tw_rut_habiente,tw_dv_habiente);
	printf("%s|%s|%s|%s %s|",tw_est_trj,fec2,tw_cod_bloq_trj,fec3,hra);
	printf("%s|%s|%s|%s|%s|",tw_num_cta,tw_logo_trj,tw_rut_titular,tw_dv_titular,tw_nom_titular);
	printf("%s|%s|%s|%s|%s\n",tw_dir_env_est_cta,tw_com_env_est_cta,tw_ciu_env_est_cta,tw_reg_env_est_cta,tw_cod_blq_cta);
}
' ${DirTmp}/arch_trj_wlt_ORD > ${DirTmp}/arch_trj_wlt_format


printlog "...Formateo de archivo OK"
Mensaje "...Formateo de archivo OK"


printlog "Elimina caracteres"
Mensaje "Elimina caracteres"

sed -e 's/;//g' ${DirTmp}/arch_trj_wlt_format > ${DirTmp}/arch_trj_wlt
printlog "...Elimina caracteres OK"
Mensaje "...Elimina caracteres OK"


Realiza_BcpIn $bd tw_tarjeta_wallet ${DirTmp}/arch_trj_wlt "|"

Eliminar_Archivo ${DirTmp}/arch_trj_wlt_ORD
Eliminar_Archivo ${DirTmp}/arch_trj_wlt_format
Eliminar_Archivo ${DirTmp}/arch_trj_wlt

Eliminar_Archivo ${DirIn}/${arch_ibm}_${FecArch}.${ext_ibm}

Mensaje ""
Mensaje "Fin shell WLT_Carga_Maestro_Cuenta.sh"
date '+%Y-%m-%d %H:%M:%S' >> $sLOG/$ARCHLOG

printlog ""
printlog "Hora de Termino "$Hora
printlog " Fin Shell WLT_Carga_Maestro_Cuenta.sh"
SALIR 0 "OK"
